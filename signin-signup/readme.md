Intro
======
User Signin Signup Demo project.

Configuration
==============

Database used : MySQL
Schema Name: user_resgitration
Database username: root
Database password: root
Database port: 3306

Change the username,password and port for MySQL as per your local MySQL configuration to make run application successfully.

Use create_db.sql file from database folder under resources to create table.

Steps to Run Project:
=====================
1. Import project into Eclipse/STS or Any other IDE.
2. Build the application to download all required dependencies
3. After build successfully Run the application.
4. Once server started visit following URL http://localhost:8080swagger-ui.html in browser
5. Application port is 8080.


Test Scenarios
==============
1) For Registration use following API
/user/auth/signup
with following json to create user
{
    "username": "test",
    "email": "test@gmail.com",
    "password": "sample"
}

After user is created successfully following message will display
{
    "success": true,
    "message": "User registered successfully"
}

2) For Login with username of registered user use following API
/user/auth/signin
with following json to login
{
    "usernameOrEmail": "test",
    "password": "sample"
}
After successful login JWT token will return like this
{
    "accessToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNTk1MTgzNDU2LCJleHAiOjE1OTU3ODgyNTZ9.wdx-oFTDjU_1MFQ0_Ccat1pIb8PA-he1avL2sZ5gwb6LGjoJ9JyTcBxRGsXHZE59Z8S7yIqVkFPcJ1xTHDlglw",
    "tokenType": "Bearer"
}

3) Create another user with following JSON with signup API
{
    "username": "user1",
    "email": "user1@gmail.com",
    "password": "user1@123"
}

4) For Login with email of registered user use following API 
/user/auth/signin
{
    "usernameOrEmail": "user1@gmail.com",
    "password": "user1@123"
}
After successful login JWT token will return like this
{
    "accessToken": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyIiwiaWF0IjoxNTk1MTgzNDU2LCJleHAiOjE1OTU3ODgyNTZ9.wdx-oFTDjU_1MFQ0_Ccat1pIb8PA-he1avL2sZ5gwb6LGjoJ9JyTcBxRGsXHZE59Z8S7yIqVkFPcJ1xTHDlglw",
    "tokenType": "Bearer"
}

All users which are successfully created will stored into DB.
So next time just can login.

Testing on AWS
==============
Use following URL to test same application on AWS
http://usersignupsignin-env.eba-untwyxmu.us-east-2.elasticbeanstalk.com/swagger-ui.html

Test Scenario's will be same.